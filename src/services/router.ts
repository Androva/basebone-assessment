import * as express from "express";
import Categories from "../schemas/categories.schema";
import Locale from "../schemas/locale.schema";
import Locks from "../schemas/locks.schema";
import Media from "../schemas/media.schema";
import Settings from "../schemas/settings.schema";
import Database from "../services/databaseConnector";

let router = express.Router();

router.patch('/', (req, res) => {
    const information = req.body;
    if (req.body.length < 1)
        res.json({ success: true })

    const collection = information.collection;
    const fields = information.fields;

    if (!collection) {
        res.status(400)
            .json({
                error: 'Collection not specified',
                success: true
            })
    }
    if (!fields?.id) {
        res.status(400)
            .json({
                error: 'id field not specified',
                success: true
            })
    }

    const database = new Database;
    const update = database.update(fields, collection);

    res.json({ success: true })
});

router.get('/', pagination(), (req, res: any) => {
    res.json(res.pagination)
})

function pagination() {
    return async (req: any, res: any, next: any) => {
        const collection: string = req.query.collection;
        const page: number = parseInt(req.query.page);
        const pageToSkip: number = (page - 1) * 2;
        const results: any = {};

        if (!collection)
            res.status(400)
                .json({ successs: true, error: 'Collection not specified' })

        if (!page)
            res.status(400)
                .json({ successs: true, error: 'Page number not specified' })

        try {
            switch (collection) {
                case 'Locale':
                    console.log('Locale')
                    results.results = await Locale.find()
                        .sort({ _id: 1 })
                        .limit(2)
                        .skip(pageToSkip)
                        .exec();
                    res.pagination = results;
                    next();
                    break;
                case 'Settings':
                    results.results = await Settings.find()
                        .sort({ _id: 1 })
                        .limit(10)
                        .skip(pageToSkip)
                        .exec();
                    res.pagination = results;
                    next();
                    break;
                case 'Media':
                    results.results = await Media.find()
                        .sort({ _id: 1 })
                        .limit(10)
                        .skip(pageToSkip)
                        .exec();
                    res.pagination = results;
                    next();
                    break;
                case 'Locks':
                    results.results = await Locks.find()
                        .sort({ _id: 1 })
                        .limit(10)
                        .skip(pageToSkip)
                        .exec();
                    res.pagination = results;
                    next();
                    break;
                case 'Categores':
                    results.results = await Categories.find()
                        .sort({ _id: 1 })
                        .limit(10)
                        .skip(pageToSkip)
                        .exec();
                    res.pagination = results;
                    next();
                    break;

                default:
                    break;
            }

        } catch (e) {
            res.status(500).json({ message: "Error Occured" });
        }
    };
}

export default router;