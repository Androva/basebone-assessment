import mongoose from 'mongoose';

import Settings from '../schemas/settings.schema';
import Media from '../schemas/media.schema';
import Locks from '../schemas/locks.schema';
import Locale from '../schemas/locale.schema';
import Categories from '../schemas/categories.schema';

export default class Database {
    connect(server: string, database: string) {
        mongoose.connect(`mongodb://${server}/${database}`)
            .then(() => {
                console.log(`Succesfully connect to database (${database}).`)
            })
            .catch((error) => {
                console.error(`Could not connect to database instance: ${error}`)
            })
    }


    generateId(length: number) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

    async initialise() {
        // Settings Objects
        const settingsPremium13 = new Settings({ is_premium: true, age_rating: '13' })
        const settingsPremium18 = new Settings({ is_premium: true, age_rating: '18' })
        const settingsNoPremiumAll = new Settings({ is_premium: false, excluded_domains: ['.co.za', '.co.uk', '.co.ro'], excluded_countries_iso: ['ZAF', 'GBR', 'ROU'], excluded_network_endpoints: [123, 322, 698], age_rating: '13' })
        // Save objects
        await settingsPremium13.save();
        await settingsPremium18.save();
        await settingsNoPremiumAll.save();

        // Media Objects
        const mediaAll = new Media({ icon: 'icon.png', portait: ['1.png', '2.png', '3.png', '4.png'], landscape: ['5.png', '6.png', '7.png', '8.png'], sqaure: ['9.png', '10.png', '11.png', '12.png'] })
        const media1 = new Media({ icon: 'icon.png' })
        const media2 = new Media({ icon: 'icon.png', landscape: ['5.png', '6.png', '7.png', '8.png'] })
        const media3 = new Media({ portait: ['1.png', '2.png', '3.png', '4.png'], landscape: ['5.png', '6.png', '7.png', '8.png'], sqaure: ['9.png', '10.png', '11.png', '12.png'] })
        // Save objects
        await mediaAll.save();
        await media1.save();
        await media2.save();
        await media3.save();

        // Locale Objects
        const localeWithSEO = new Locale({
            language_iso: 'eng',
            title: 'English',
            seo_title: 'English Language',
            summary: 'The English Language',
            seo_summary: 'The English Language',
            description: 'A much longer way to say "The English Language"',
            seo_description: 'A much longer way to say "The English Language"',
            specify_seo_values: true
        });
        const localeNoSEO = new Locale({
            language_iso: 'sot',
            title: 'Southern seSotho',
            seo_title: 'Southern seSotho Language',
            summary: 'The Southern seSotho Language',
            seo_summary: 'The Southern seSotho Language',
            description: 'A much longer way to say "The Southern seSotho Language"',
            seo_description: 'A much longer way to say "The Southern seSotho Language"',
            specify_seo_values: false
        });
        const localeWithoutSEO = new Locale({
            language_iso: 'rus',
            title: 'Russian',
            summary: 'The Russian Language',
            description: 'A much longer way to say "The Russian Language"',
            specify_seo_values: false
        });
        // Save objects
        await localeWithSEO.save();
        await localeNoSEO.save();
        await localeWithoutSEO.save();

        // Locks Objects
        const locksAll = new Locks({
            is_locked_for_editing: 'Yes',
            current_editor: 'Naledi',
            is_locked_for_moderation_process: 'No',
            is_locked_for_backend_process: 'No',
            current_backend_process: 'None',
        });
        const locks1 = new Locks({
            is_locked_for_editing: 'Yes'
        });
        const locks2 = new Locks({
            is_locked_for_editing: 'Yes',
            current_editor: 'Naledi'
        });
        const locks3 = new Locks({
            is_locked_for_editing: 'No',
            current_editor: 'None',
            is_locked_for_moderation_process: 'Yes',
        });
        const locks4 = new Locks({
            is_locked_for_editing: 'No',
            current_editor: 'None',
            is_locked_for_moderation_process: 'Yes',
            is_locked_for_backend_process: 'No',
        });
        // Save objects
        await locksAll.save();
        await locks1.save();
        await locks2.save();
        await locks2.save();
        await locks4.save();

        // Categories Objects
        const newRussianCategoryAll = new Categories({
            id: this.generateId(12),
            slug: this.generateId(24),
            locale: localeWithSEO,
            media: mediaAll,
            settings: settingsNoPremiumAll,
            locks: locks2,
            parent_id: this.generateId(12),
            ancestors_id: [this.generateId(12), this.generateId(12), this.generateId(12)],
            product: 'Book',
            path: './books/Book',
            is_indexed: true,
            published_at: new Date('2020/08/01'),
            created_at: new Date('2020/06/21'),
            updated_at: new Date('2020/08/01'),
        });
        const newEnglishCategoryRequired = new Categories({
            id: this.generateId(12),
            slug: this.generateId(24),
            locale: localeWithoutSEO,
            media: media3,
            settings: settingsPremium13,
            locks: locksAll,
            is_indexed: true,
        });
        const newSothoCategorySome = new Categories({
            id: this.generateId(12),
            slug: this.generateId(24),
            locale: localeWithSEO,
            media: mediaAll,
            settings: settingsNoPremiumAll,
            locks: locks2,
            product: 'Game',
            is_indexed: false,
            created_at: new Date('2021/02/15'),
            updated_at: new Date('2020/10/30'),
        });
        // Save objects
        await newRussianCategoryAll.save();
        await newSothoCategorySome.save();
        await newEnglishCategoryRequired.save();
    }

    update(fields: any, collectionName: string) {
        switch (collectionName) {
            case 'Locale':
                Locale.findOneAndUpdate({ _id: fields.id }, fields, { new: true }, (error, doc) => {
                    if (error)
                        return false;
                    else 
                        return (doc)
                })
                break;
            case 'Settings':
                Settings.findOneAndUpdate({ _id: fields.id }, fields, { new: true }, (error, doc) => {
                    if (error)
                        return (false)
                    else
                        return (doc)
                })
                break;
            case 'Media':
                Media.findOneAndUpdate({ _id: fields.id }, fields, { new: true }, (error, doc) => {
                    if (error)
                        return (false)
                    else
                        return (doc)
                })
                break;
            case 'Locks':
                Locks.findOneAndUpdate({ _id: fields.id }, fields, { new: true }, (error, doc) => {
                    if (error)
                        return (false)
                    else
                        return (doc)
                })
                break;
            case 'Categories':
                Categories.findOneAndUpdate({ _id: fields.id }, fields, { new: true }, (error, doc) => {
                    if (error)
                        return (false)
                    else
                        return (doc)
                })
                break;
            default:
                return (false)
        }
    }
}