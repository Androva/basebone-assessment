import * as dotenv from "dotenv";
import express from "express";
import cors from "cors";
import Database from "./services/databaseConnector";
import router from "./services/router"

dotenv.config();

if (!process.env.PORT || !process.env.DB_SERVER || !process.env.DB) {
    process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);
const server: string = process.env.DB_SERVER;
const db: string = process.env.DB;

const app = express();

app.use(cors());
app.use(express.json());

app.use('/api', router);

app.listen(PORT, () => {
    const database = new Database;

    console.log(`Listening on port ${PORT}`);
    database.connect(server, db);

    // Uncomment below to create dummy data
    /* database.initialise()
        .then(() => {
            console.log('Collections created.')
        })
        .catch((error) => {
            console.error(`Error creating collections: ${error}`)
        }) */
});
