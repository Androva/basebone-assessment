import mongoose from 'mongoose';

const settingsSchema = new mongoose.Schema({
    is_premium: {type: Boolean, required: true},
    excluded_domains: [String],
    excluded_countries_iso: [String],
    excluded_network_endpoints: [Number],
    age_rating: {type: String, required: true},
});

const Settings = mongoose.model('Settings', settingsSchema, 'Settings');

export default Settings;