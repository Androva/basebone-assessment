import mongoose from 'mongoose';

import Locale from './locale.schema';
import Settings from './settings.schema';
import Media from './media.schema';
import Locks from './locks.schema';

const categoriesSchema = new mongoose.Schema({
    id: { type: String, required: true },
    slug: { type: String, required: true, unique: true },
    locale: { type: mongoose.Schema.Types.ObjectId, ref: Locale, required: true },
    media: { type:  mongoose.Schema.Types.ObjectId, ref: Media, required: true },
    settings: { type: mongoose.Schema.Types.ObjectId, ref: Settings, required: true },
    locks: { type:  mongoose.Schema.Types.ObjectId, ref: Locks, required: true },
    parent_id: String,
    ancestors_id: [String],
    product: String,
    path: String,
    is_indexed: { type: Boolean, required: true },
    published_at: Date,
},
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

const Categories = mongoose.model('Categories', categoriesSchema, 'Categories');

export default Categories;