import mongoose from 'mongoose';

const localeSchema = new mongoose.Schema({
    language_iso: { type: String, required: true },
    title: { type: String, required: true },
    seo_title: String,
    summary: { type: String, required: true },
    seo_summary: String,
    description: { type: String, required: true },
    seo_description: String,
    specify_seo_values: { type: Boolean, required: true }
});

const Locale = mongoose.model('Locale', localeSchema, 'Locale');

export default Locale;