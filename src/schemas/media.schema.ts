import mongoose from 'mongoose';

const mediaSchema = new mongoose.Schema({
    icon: String,
    portrait: [String],
    landscape: [String],
    square: [Number],
});

const Media = mongoose.model('Media', mediaSchema, 'Media');

export default Media;