import mongoose from 'mongoose';

const locksSchema = new mongoose.Schema({
    is_locked_for_editing: String,
    current_editor: String,
    is_locked_for_moderation_process: String,
    is_locked_for_backend_process: String,
    current_backend_process: String,
});

const Locks = mongoose.model('Locks', locksSchema, 'Locks');

export default Locks;